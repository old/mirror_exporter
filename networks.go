package main

import (
   "net"
)

var (
   Networks = map[string][]net.IPNet{
      "uwaterloo": []net.IPNet{
         net.IPNet{ net.ParseIP("10.0.0.0"), net.CIDRMask(8, 32), },
         net.IPNet{ net.ParseIP("129.97.0.0"), net.CIDRMask(16, 32), },
         net.IPNet{ net.ParseIP("172.16.0.0"), net.CIDRMask(12, 32), },
         net.IPNet{ net.ParseIP("192.168.0.0"), net.CIDRMask(16, 32), },
         net.IPNet{ net.ParseIP("2620:101:f000::"), net.CIDRMask(47, 128), },
         net.IPNet{ net.ParseIP("fd74:3ae8:4dde::"), net.CIDRMask(47, 128), },
      },
   }
)

func IdentifyNetwork(ip net.IP) string {
   for name, nets := range Networks {
      for _, net := range nets {
         if net.Contains(ip) {
            return name
         }
      }
   }

   return "other"
}

func IdentifyIPProtocol(ip net.IP) string {
   if ip.To4() != nil {
      return "ipv4"
   } else if ip.To16() != nil {
      return "ipv6"
   }

   return "other"
}
