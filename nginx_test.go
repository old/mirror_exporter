package main

import (
   "testing"
)

func testCreate(t *testing.T, path string) {
   exporter, err := NewNginxExporter(path)

   if err != nil {
      t.Fatal("err was not nil.", err)
   }

   // Check path on exporter
   if exporter.AccessLogPath != path {
      t.Fatalf("Wrong path in Nginx Exporter object. Expected %s, got %s", path, exporter.AccessLogPath)
   }
}

func TestCreateNginxExporter(t *testing.T) {
  testCreate(t, "/var/log/nginx/access.log")
}

func TestCreateNginxExporterWithNonStandardNginxPath(t *testing.T) {
   testCreate(t, "/tmp/nginx.log")
}
