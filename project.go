package main

import (
   "fmt"
   "os"
)

func IsMirroredProject(project string) bool {
   if _, err := os.Stat(fmt.Sprintf("/mirror/root/%s", project)); err == nil {
      return project != "pub"
   } else {
      return false
   }
}
